package com.pwootage.palendrome;

public class Palendrome implements Comparable<Palendrome> {
	public int start;
	public int length;
	
	public Palendrome(int start, int length) {
		this.start = start;
		this.length = length;
	}

	@Override
	public int compareTo(Palendrome o) {
		if (length == o.length) return 0;
		if (length > o.length) return -1;
		return 1;
	}

	@Override
	public String toString() {
		return "Palendrome [start=" + start + ", length=" + length + "]";
	}
}

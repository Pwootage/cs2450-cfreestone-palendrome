package com.pwootage.palendrome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class PalendromeFinder {
	private static Scanner in;

	public static void main(String[] args) {
		in = new Scanner(System.in);

		String input = getInput();
		PalendromeFinder pf = new PalendromeFinder(input);
		pf.findPalendromes();
		System.out.println("Longest: " + pf.getLongest());
	}

	public static String getInput() {
		System.out.print("Please enter a text to find palendromes: ");
		return in.nextLine();
	}

	private String str;
	private ArrayList<Palendrome> palendromes;

	public PalendromeFinder(String str) {
		in = new Scanner(System.in);
		palendromes = new ArrayList<>();
		this.str = str.toUpperCase();
	}

	public void findPalendromes() {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (str.length() > i + 1 && str.charAt(i + 1) == c) {
				palendromes.add(new Palendrome(i, 2));
			}
			if (str.length() > i + 2 && str.charAt(i + 2) == c) {
				palendromes.add(new Palendrome(i, 3));
			}
		}
		for (Palendrome p : palendromes) {
			extendPalendrome(p);
		}
	}

	private void extendPalendrome(Palendrome p) {
		int start = p.start;
		int length = p.length;

		while (start >= 0 && start + length - 1 < str.length()
				&& str.charAt(start) == str.charAt(start + length - 1)) {
			start--;
			length += 2;
		}
		if (start < p.start) {
			p.start = start + 1;
		}
		if (length > p.length) {
			p.length = length - 1;
		}
	}

	public String getLongest() {
		Collections.sort(palendromes);
		int start = palendromes.get(0).start;
		int end = palendromes.get(0).start + palendromes.get(0).length - 1;
		return str.substring(start, end);
	}
}
